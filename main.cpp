#include <QApplication>
#include "mainwindow.h"

int main( int argc, char *argv[] )
{
#if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
    QApplication::setAttribute( Qt::AA_EnableHighDpiScaling );
#endif

    QApplication app( argc, argv );

    MainWindow w;
    w.resize( 800, 600 );
    w.showMaximized();

    return app.exec();
}
