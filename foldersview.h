#ifndef FOLDERSVIEW_H
#define FOLDERSVIEW_H

#include <QWidget>
#include <QDir>

namespace Ui
{
class FoldersView;
}

class FoldersView : public QWidget
{
    Q_OBJECT

public:
    explicit FoldersView( QWidget *parent = nullptr );
    ~FoldersView();

private slots:
    void onRootDirChanged( const QString &dir );

private:
    void set_crumbs( const QDir &dir );

    Ui::FoldersView *ui;
};

#endif // FOLDERSVIEW_H
