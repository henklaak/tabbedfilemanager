#ifndef FOLDERVIEW_H
#define FOLDERVIEW_H

#include <QWidget>
#include <QFileSystemModel>
#include <foldersorter.h>
#include <myfilesystemmodel.h>

namespace Ui
{
class FolderView;
}

class FolderView : public QWidget
{
    Q_OBJECT

public:
    explicit FolderView( QWidget *parent = nullptr );
    ~FolderView();
    void set_root_by_index( const QModelIndex &index );

    QString rootDir();

signals:
    void rootDirChanged( const QString &dir );

protected:
    void resizeEvent( QResizeEvent *event );
    void keyPressEvent( QKeyEvent *event );


private slots:
    void on_directoryLoaded( const QString &path );
    void on_expanded();
    void on_collapsed();

private:
    bool eventFilter(QObject *object, QEvent *event);

    void autoresize_columns();
    void set_crumbs( const QDir &dir );
    Ui::FolderView *ui;
    MyFileSystemModel m_model;
    FolderSorter m_sorted;
};

#endif // FOLDERVIEW_H
