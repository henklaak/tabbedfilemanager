#include "foldersview.h"
#include "ui_foldersview.h"
#include "pathlineedit.h"

FoldersView::FoldersView( QWidget *parent ) :
    QWidget( parent ),
    ui( new Ui::FoldersView )
{
    ui->setupUi( this );
    ui->lineeditPath->setText( "hoi" );

    QObject::connect( ui->folderLeft, &FolderView::rootDirChanged,
                      this, &FoldersView::onRootDirChanged );
    QObject::connect( ui->folderRight, &FolderView::rootDirChanged,
                      this, &FoldersView::onRootDirChanged );
}

FoldersView::~FoldersView()
{
    delete ui;
}

void FoldersView::set_crumbs( const QDir &dir )
{
    qDeleteAll( ui->frame_crumbs->findChildren<QPushButton *>() );

    auto frm = ui->frame_crumbs;
    auto pb = new QPushButton( dir.path(), frm );
    ui->horizontalLayout->insertWidget( 0, pb );
}


void FoldersView::onRootDirChanged( const QString &dir )
{
    ui->lineeditPath->setText( dir );
}

