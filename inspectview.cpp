#include "inspectview.h"
#include "ui_inspectview.h"

InspectView::InspectView(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::InspectView)
{
    ui->setupUi(this);
}

InspectView::~InspectView()
{
    delete ui;
}
