#ifndef INSPECTVIEW_H
#define INSPECTVIEW_H

#include <QWidget>

namespace Ui {
class InspectView;
}

class InspectView : public QWidget
{
    Q_OBJECT

public:
    explicit InspectView(QWidget *parent = nullptr);
    ~InspectView();

private:
    Ui::InspectView *ui;
};

#endif // INSPECTVIEW_H
