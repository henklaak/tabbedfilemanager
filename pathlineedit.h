#ifndef PATHLINEEDIT_H
#define PATHLINEEDIT_H

#include <QLineEdit>

class PathLineEdit : public QLineEdit
{
    Q_OBJECT
public:
    PathLineEdit(QWidget *parent = nullptr);
};

#endif // PATHLINEEDIT_H
