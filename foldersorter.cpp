#include "foldersorter.h"

#include <QFileSystemModel>

FolderSorter::FolderSorter( QObject *parent )
    : QSortFilterProxyModel( parent )
{
}

FolderSorter::~FolderSorter()
{

}

bool FolderSorter::lessThan( const QModelIndex &left,
                             const QModelIndex &right ) const
{
    if( 1 )
    {
        QFileSystemModel *fsm = qobject_cast<QFileSystemModel *>( sourceModel() );
        bool asc = sortOrder() == Qt::AscendingOrder ? true : false;

        QFileInfo leftFileInfo  = fsm->fileInfo( left );
        QFileInfo rightFileInfo = fsm->fileInfo( right );

        // If DotAndDot move in the beginning
        if( sourceModel()->data( left ).toString() == ".." )
        {
            return asc;
        }

        if( sourceModel()->data( right ).toString() == ".." )
        {
            return !asc;
        }

        // Prefer dirs first
        if( !leftFileInfo.isDir() && rightFileInfo.isDir() )
        {
            return !asc;
        }

        if( leftFileInfo.isDir() && !rightFileInfo.isDir() )
        {
            return asc;
        }
    }

    return QSortFilterProxyModel::lessThan( left, right );
}
