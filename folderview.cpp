#include <QFileSystemModel>
#include <QKeyEvent>
#include <QPushButton>
#include <QTimer>
#include <iostream>
#include "folderview.h"
#include "ui_folderview.h"

FolderView::FolderView( QWidget *parent ) :
    QWidget( parent ),
    ui( new Ui::FolderView )
{
    ui->setupUi( this );

    connect( &m_model, &QFileSystemModel::directoryLoaded,
             this, &FolderView::on_directoryLoaded );
    connect( ui->treeView, &QTreeView::expanded,
             this, &FolderView::on_expanded );
    connect( ui->treeView, &QTreeView::collapsed,
             this, &FolderView::on_collapsed );

    ui->treeView->installEventFilter( this );

    m_model.setRootPath( QDir::homePath() );
    m_sorted.setSourceModel( &m_model );

    ui->treeView->setModel( &m_sorted );
    ui->treeView->sortByColumn( 0, Qt::AscendingOrder );

}

FolderView::~FolderView()
{
    delete ui;
}

QString FolderView::rootDir()
{
    QString file_path = m_model.filePath( m_sorted.mapToSource( ui->treeView->rootIndex() ) );
    return file_path;
}

void FolderView::set_root_by_index( const QModelIndex &index )
{
    ui->treeView->collapseAll();
    ui->treeView->setRootIndex( index );
    autoresize_columns();

    QString file_path = m_model.filePath( m_sorted.mapToSource( index ) );
    emit rootDirChanged( file_path );
}

void FolderView::resizeEvent( QResizeEvent *event )
{
    QWidget::resizeEvent( event );
}

void FolderView::keyPressEvent( QKeyEvent *event )
{
    if( event->key() == Qt::Key_Return )
    {
        QModelIndex current = ui->treeView->currentIndex();
        set_root_by_index( current );
    }
    else if( event->key() == Qt::Key_Escape )
    {
        QModelIndex current = ui->treeView->rootIndex().parent();
        set_root_by_index( current );
    }
    else
    {
        QWidget::keyPressEvent( event );
    }
}

void FolderView::on_directoryLoaded( const QString &path )
{
    autoresize_columns();
}
void FolderView::on_expanded()
{
    autoresize_columns();
}
void FolderView::on_collapsed()
{
    autoresize_columns();
}

void FolderView::autoresize_columns()
{
    for( int c = 0; c < m_sorted.columnCount(); ++c )
    {
        ui->treeView->resizeColumnToContents( c );
    }
}

bool FolderView::eventFilter( QObject *object, QEvent *event )
{
    if( event->type() == QEvent::FocusIn )
    {
        emit rootDirChanged( rootDir() );
    }
    if( event->type() == QEvent::FocusOut )
    {
        ui->treeView->selectionModel()->clear();
    }
    return false;
}
