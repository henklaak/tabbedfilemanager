#ifndef FOLDERSORTER_H
#define FOLDERSORTER_H

#include <QSortFilterProxyModel>

class FolderSorter : public QSortFilterProxyModel
{
    Q_OBJECT
public:
    explicit FolderSorter( QObject *parent = nullptr );
    ~FolderSorter();

private:
    bool lessThan( const QModelIndex &left, const QModelIndex &right ) const;
};

#endif // FOLDERSORTER_H
