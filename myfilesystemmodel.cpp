#include "myfilesystemmodel.h"

MyFileSystemModel::MyFileSystemModel( QObject *parent )
    : QFileSystemModel( parent )
{

}

QVariant MyFileSystemModel::headerData( int section, Qt::Orientation orientation, int role ) const
{
    QVariant result = QFileSystemModel::headerData( section, orientation, role );
    return result;
}

